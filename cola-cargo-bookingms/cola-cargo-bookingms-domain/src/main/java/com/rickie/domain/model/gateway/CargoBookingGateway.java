package com.rickie.domain.model.gateway;

import com.rickie.domain.model.aggregates.Cargo;
import com.rickie.domain.model.valueobjects.LastCargoHandledEvent;

/**
 * @author rickie
 * @version 1.0.0
 * @ClassName CargoGateway.java
 * @Description TODO
 * @createTime 2021年06月14日 00:48:00
 */
public interface CargoBookingGateway {
    public void save(Cargo cargo);
    public void update(Cargo cargo);
    public void updateRoutingStatus(Cargo cargo);
    public Cargo getByBookingId(String bookingId);
    public void updateByLastCargoHandledEvent(LastCargoHandledEvent event);
}

